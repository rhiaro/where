<?
$wheres = array( 
		array("location"=>"volunteer","published"=>strtotime("16th April 2015, 12:03"))
		,array("location"=>"transit","published"=>strtotime("16th April 2015, 11:47"))
		,array("location"=>"meeting","published"=>strtotime("16th April 2015, 11:00"))
		,array("location"=>"office","published"=>strtotime("16th April 2015, 10:45"))
		,array("location"=>"transit","published"=>strtotime("16th April 2015, 10:29"))
		,array("location"=>"home","published"=>strtotime("16th April 2015, 01:13"))
		,array("location"=>"transit","published"=>strtotime("16th April 2015, 00:49"))
		,array("location"=>"office","published"=>strtotime("15th April 2015, 20:00"))
		,array("location"=>"transit","published"=>strtotime("15th April 2015, 19:53"))
		,array("location"=>"restaurant","published"=>strtotime("15th April 2015, 19:37"))
		,array("location"=>"transit","published"=>strtotime("15th April 2015, 19:30"))
		,array("location"=>"office","published"=>strtotime("15th April 2015, 17:00"))
		,array("location"=>"meeting","published"=>strtotime("15th April 2015, 16:00"))
		,array("location"=>"office","published"=>strtotime("15th April 2015, 15:44"))
		,array("location"=>"transit","published"=>strtotime("15th April 2015, 15:35"))
		,array("location"=>"volunteer","published"=>strtotime("15th April 2015, 11:58"))
		,array("location"=>"transit","published"=>strtotime("15th April 2015, 11:48"))
		,array("location"=>"office","published"=>strtotime("15th April 2015, 11:45"))
		,array("location"=>"transit","published"=>strtotime("15th April 2015, 11:34"))
		,array("location"=>"home","published"=>strtotime("15th April 2015, 07:30"))
		,array("location"=>"exercise","published"=>strtotime("15th April 2015, 06:50"))
		,array("location"=>"home","published"=>strtotime("14th April 2015, 22:30"))
		,array("location"=>"transit","published"=>strtotime("14th April 2015, 22:10"))
		,array("location"=>"office","published"=>strtotime("14th April 2015, 19:00"))
		,array("location"=>"meeting","published"=>strtotime("14th April 2015, 18:00"))
		,array("location"=>"office","published"=>strtotime("14th April 2015, 16:40"))
		,array("location"=>"transit","published"=>strtotime("14th April 2015, 16:20"))
		,array("location"=>"volunteer","published"=>strtotime("14th April 2015, 12:00"))
	);

$sentences = array(
				 "office" => "amy is in her office"
				,"home" => "amy is at home"
				,"volunteer" => "amy is out helping with something"
				,"event" => "amy is at an event"
				,"restaurant" => "amy is out getting food"
				,"exercise" => "amy is out exercising"
				,"meeting" => "amy is in a meeting"
				,"seminar" => "amy is in a talk or seminar"
				,"transit" => "amy is in transit"
				,"other" => "amy's whereabouts are currently unspecified"
	);

function hours_and_minutes($minutes){
	$m = $minutes % 60;
	if($m > 1) $ms = "s"; else $ms = "";
	if($m > 0) $mt = "$m minute$ms"; else $mt = "";
	$h = floor($minutes / 60);
	if($h > 1) $hs = "s"; else $hs = "";
	if($h > 0) $ht = "$h hour$hs"; else $ht = "";
	return trim("$ht $mt");
}
?>
<!doctype html>
<html>
<head>
	<title>Where is Amy?</title>
	<link rel="stylesheet" href="http://rhiaro.co.uk/css/normalize.min.css"/>
        <link rel="stylesheet" href="http://rhiaro.co.uk/css/main.css"/>
        <link rel="stylesheet" href="http://rhiaro.co.uk/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="where.css"/>
</head>
<body>
	<main class="where-wrapper">
		<div class="key lighter-bg">
			<div class="block office"><i class="fa fa-laptop fa-2x"></i></div>
			<div class="block home"><i class="fa fa-home fa-2x"></i></div>
			<div class="block volunteer"><i class="fa fa-gift fa-2x"></i></div>
			<div class="block event"><i class="fa fa-calendar fa-2x"></i></div>
			<div class="block restaurant"><i class="fa fa-cutlery fa-2x"></i></div>
			<div class="block exercise"><i class="fa fa-heartbeat fa-2x"></i></div>
			<div class="block meeting"><i class="fa fa-pencil-square-o fa-2x"></i></div>
			<div class="block seminar"><i class="fa fa-graduation-cap fa-2x"></i></div>
			<div class="block transit"><i class="fa fa-spinner fa-2x"></i></div>
			<div class="block other"><i class="fa fa-question-circle fa-2x"></i></div>
			<span class="darker">1px = 1min</span>
		</div>
		<?for($i=0;$i<count($wheres);$i++):?>
			<?if($i-1 < 0):?>
				<? $d = (time()-$wheres[$i]['published'])/60; ?>
			<?else:?>
				<? $d = ($wheres[$i-1]['published']-$wheres[$i]['published'])/60; ?>
			<?endif?>
			<div class="w1of1 h-entry clearfix">
				<div class="w1of2"></div>
				<div class="w1of2 block <?=$wheres[$i]['location']?>" style="height: <?=$d?>px;">
					<div class="text">
						<span class="p-name"><?=$sentences[$wheres[$i]['location']]?><data class="p-location" value="<?=$wheres[$i]['location']?>"></data></span>
						<time class="dt-published" datetime="<?=date(DATE_ATOM, $wheres[$i]['published'])?>"></time>
						(<span class="dt-duration"><?=hours_and_minutes($d)?></span>)
					</div>	
				</div>
			</div>
		<?endfor?>
	</main>
</body>
</html>